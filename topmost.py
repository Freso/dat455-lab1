#!/usr/bin/env python3
"""Script to show the n most frequent words from a document."""

import sys
import wordfreq as wf

from urllib.request import urlopen


def _file_to_list(file, is_url=False):
    """Return a list consisting of all the lines in a given file."""
    if is_url:
        return file.read().decode('utf8').splitlines()
    return file.read().splitlines()


def main(args: list):
    """Print out top n words from input (web) document, excluding stopwords."""
    if args[1].split('://', 1)[0] in ('http', 'https'):
        with urlopen(args[1]) as response:
            tokens = wf.tokenize(_file_to_list(response, True))
    else:
        with open(args[1]) as inp_file:
            tokens = wf.tokenize(_file_to_list(inp_file))
    with open(args[0]) as stop_words_file:
        stop_words = _file_to_list(stop_words_file)
    word_frequency = wf.count_words(tokens, stop_words)
    wf.print_top_most(word_frequency, int(args[2]))


if __name__ == '__main__':
    main(sys.argv[1:])
