"""Module for handling word frequency discovery."""


def tokenize(lines: list):
    """Return list of tokens from provided list of text lines."""
    words = []
    for line in lines:
        line = line.strip()  # Get rid of any excessive whitespace.
        start = 0
        while start < len(line):
            while line[start].isspace():
                start = start + 1
            end = start
            if line[start].isalpha():
                while end < len(line) and line[end].isalpha():
                    end = end + 1
            elif line[start].isdigit():
                while end < len(line) and line[end].isdigit():
                    end = end + 1
            else:
                end = end + 1
                # This commented out handling will catch cases
                # like "..." and "?!" as a group instead of as
                # individual items.
                #
                # while end < len(line) and not (
                #         line[end].isalpha() or
                #         line[end].isdigit() or
                #         line[end].isspace()):
                #     end = end + 1
            words.append(line[start:end].lower())
            start = end
    return words


def count_words(words: list, stop_words: list):
    """Return a dict of provided words and their frequency."""
    frequencies = {}
    for word in words:
        if word in stop_words:
            pass
        else:
            if word in frequencies:
                frequencies[word] += 1
            else:
                frequencies[word] = 1
    return frequencies


def print_top_most(frequencies: dict, n: int):
    """Print the top n most frequent words from provided dict."""
    top = 0
    for word, freq in sorted(frequencies.items(), key=lambda x: -x[1]):
        if top >= n:
            break
        print('{:<20}{:>5}'.format(word, freq))
        top += 1


# camelCase is #NotPythonic for function/method names
countWords, printTopMost = (count_words, print_top_most)
